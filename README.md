# dotfiles v2: nord edition

my dotfiles, nord this time.

for my older, unused dotfiles see: https://codeberg.org/kantraa/dotfiles

# screenshots
<img src="http://0x0.st/oThW.png">
<img src="http://0x0.st/oThJ.png">

theme: nordic-theme on AUR

icon pack: tela-circle-nord-dark

bar: polybar

launcher: rofi

wm: bspwm